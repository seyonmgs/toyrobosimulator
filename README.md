Toy Robot Simulator
===================

Overview
-----------

This is a command line JAVA application built using GRADLE, simulating a toy robot moving on a rectangular (including squares) tabletop,
of default dimensions 5 units x 5 units. This application is case-sensitive.


Compatibility
-----------

- Requires at the minimum Java 8.


Usage
------

The application can be started up using the following command:

    ./gradlew startApp --console plain

Constraints
-----------

The constraints can be specified through JVM arguments. When none are provided, the application will default to
5 units x 5 units.

Responses
-----------

By application by default will provide only those responses as stated in the PROBLEM statement.
However, DEBUG level responses can be enabled via JVM arguments.

In standard response level, a response is provided only if the command meets all the criteria defined in PROBLEM statement.
If DEBUG response level, a response is always provided to indicate if the command was accepted if it met all criteria. 
If not, the contraint in violation is reported back to the user.

JVM Arguments
------------------------
The JVM arguments can be specified by modifying the startApp task or via the standard java -jar command.

```groovy
task startApp(type: Exec) {
    dependsOn jar
    standardInput = System.in
    group = "execution"
    description = "Run the output executable jar with ExecTask"
    commandLine "java", "-jar",
            "-DCLTRS_RESPONSE_LEVEL=DEBUG",
            "-DCLTRS_MAX_ABSCISSA=10",
            "-DCLTRS_MIN_ABSCISSA=-5",
            "-DCLTRS_MAX_ORDINATE=8",
            "-DCLTRS_MIN_ORDINATE=-10",
            jar.archiveFile.get()
}
```




### Example a (without DEBUG)

    PLACE 0,0,NORTH
    MOVE
    REPORT

Expected output:

    0,1,NORTH

### Example a (with DEBUG)

    PLACE 0,0,NORTH
    MOVE
    REPORT

Expected output:
    
    Command accepted
    Command accepted
    0,1,NORTH
    
### Example b (without DEBUG)

    PLACE 0,0,NORTH
    LEFT
    REPORT

Expected output:

    0,0,WEST

### Example b (with DEBUG)

    PLACE 0,0,NORTH
    LEFT
    REPORT

Expected output:

    Command accepted
    Command accepted
    0,0,WEST
    
### Example c (without DEBUG)

    PLACE 1,2,east

Expected output - none


### Example c (with DEBUG)

    PLACE 1,2,east

Expected output

    Direction provided is invalid.
    

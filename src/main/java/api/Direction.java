package api;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * This Enum represents the directions that an entity is allowed to face
 * on a two dimensional plane.
 * The directions are represented in 4 Quadrants (anti-clockwise)
 * with East starting at 0 degrees.
 *
 * @see Compass
 * @see CartesianCoordinates
 */
@RequiredArgsConstructor
@Getter
public enum Direction {
    EAST(0),
    NORTH(90),
    WEST(180),
    SOUTH(270);
    private final double theta;

}

package api;

import java.util.Optional;

/**
 * This represents the action that is to performed for a 'MOVE' command.
 *
 * @see Compass
 * @see Direction
 * @see ClCommand
 * @see Orientation
 */
public class MoveCommand implements SimulatorCommand {
    /**
     * This method carries out the action that is to be performed for a 'MOVE' command {@link ClCommand}.
     * If the toyRobo {@link ToyRobo} is not placed, no further action is carried out.
     * If the move violates the constraints {@link SimulatorConstraints}, then the toyRobo remains at its current
     * orientation.
     * @param toyRobo a ToyRobo {@link ToyRobo} object.
     * @param arguments an array of arguments provided with the command, if any.
     * @param constraints a SimulatorConstraints {@link SimulatorConstraints} object.
     * @param consoleWriter a ConsoleWriter {@link ConsoleWriter} to write responses to.
     */
    @Override
    public void action(ToyRobo toyRobo, String[] arguments, SimulatorConstraints constraints, ConsoleWriter consoleWriter) {
        Optional<Orientation> orientation = toyRobo.getOrientation();

        if (!(orientation.isPresent())) {
            consoleWriter.debug("Not placed. Cannot move.");
            return;
        }

        CartesianCoordinates currentCoordinates = orientation.get().getCoordinates();
        Direction direction = orientation.get().getDirection();

        CartesianCoordinates displacement = Compass.computeDisplacement(direction);

        CartesianCoordinates newCoordinates = currentCoordinates.addCoordinates(displacement);

        if (constraints.validateCoordinates(newCoordinates)) {
            toyRobo.setOrientation(new Orientation(newCoordinates, direction));
            consoleWriter.debug("Command accepted.");
        }else {
            consoleWriter.debug("Constraint violation. Cannot move.");
        }
    }
}

package api;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * This class represents the current orientation of an entity,
 * on a two dimensional plane. This represents an entity's
 *  1) cartesian coordinates.
 *  2) direction
 *
 * @see Compass
 * @see Direction
 */
@RequiredArgsConstructor
@Getter
public class Orientation {
    @NonNull
    private final CartesianCoordinates coordinates;
    @NonNull
    private final Direction direction;
}

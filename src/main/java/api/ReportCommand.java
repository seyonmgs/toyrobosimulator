package api;

/**
 * This represents the action that is to performed for a 'REPORT' command.
 *
 * @see Compass
 * @see Direction
 * @see ClCommand
 * @see Orientation
 */
public class ReportCommand implements SimulatorCommand {
    /**
     * This method carries out the action that is to be performed for a 'REPORT' command {@link ClCommand}.
     * @param toyRobo a ToyRobo {@link ToyRobo} object.
     * @param arguments an array of arguments provided with the command, if any.
     * @param constraints a SimulatorConstraints {@link SimulatorConstraints} object.
     * @param consoleWriter a ConsoleWriter {@link ConsoleWriter} to write responses to.
     */
    @Override
    public void action(ToyRobo toyRobo, String[] arguments, SimulatorConstraints constraints, ConsoleWriter consoleWriter) {
        if (!(toyRobo.getOrientation().isPresent())){
            consoleWriter.debug("Not placed. Nothing to report.");
            return;
        }

        Orientation orientation = toyRobo.getOrientation().get();

        consoleWriter.info(orientation.getCoordinates().getAbscissa().intValue()
                +","
                + orientation.getCoordinates().getOrdinate().intValue()
                +","
                +orientation.getDirection()
        );
    }
}

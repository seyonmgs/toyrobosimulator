package api;

import lombok.Getter;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * This class represents a pair of cartesian coordinates on a two dimensional plane.
 *
 * @throws NumberFormatException if {@code abscissa, ordinate} is not a valid
 *         representation of a BigDecimal.
 *
 * @see Direction
 * @see Compass
 */
@Getter
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class CartesianCoordinates {

    /**
     * The value along x axis. This value has a scale of 2 and uses RoundingMode.HALF_UP.
     * @see BigDecimal
     * @see RoundingMode
     */
    private BigDecimal abscissa;

    /**
     * The value along y axis. This value has a scale of 2 and uses RoundingMode.HALF_UP.
     * @see BigDecimal
     * @see RoundingMode
     */
    private BigDecimal ordinate;


    /**
     * Translates a {@link BigDecimal} notation of abscissa and ordinate values into a pair
     * representing a cartesian coordinates on a 2 dimensional plane.
     * @param abscissa x axis value.
     * @param ordinate y axis value.
     */
    public CartesianCoordinates(BigDecimal abscissa, BigDecimal ordinate) {
        this.abscissa = abscissa.setScale(2, RoundingMode.HALF_UP);
        this.ordinate = ordinate.setScale(2, RoundingMode.HALF_UP);
    }

    /**
     * Translates a {@link String} notation of abscissa and ordinate values into a pair of BigDecimals
     * cartesian coordinates on a 2 dimensional plane.
     * @param abscissa String representation of x axis value {@code BigDecimal}.
     * @param ordinate String representation of y axis value {@code BigDecimal}.
     *
     * @throws NumberFormatException if {@code abscissa} or {@code ordinate}  is not a valid
     *         representation of a {@code BigDecimal}.
     */
    public CartesianCoordinates(String abscissa, String ordinate) {
        this.abscissa = new BigDecimal(abscissa, MathContext.UNLIMITED).setScale(2, RoundingMode.HALF_UP);
        this.ordinate = new BigDecimal(ordinate, MathContext.UNLIMITED).setScale(2, RoundingMode.HALF_UP);
    }


    // Arithmetic Operations
    /**
     * Returns a {@code CartesianCoordinates} whose value is {@code (this +
     * coordinates)}, and whose scale is {@code max(this.scale()}.
     *
     * @param  coordinates value to be added to this {@code CartesianCoordinates}.
     * @return {@code this + coordinates}.
     */
    public CartesianCoordinates addCoordinates(CartesianCoordinates coordinates) {
        return new CartesianCoordinates(this.abscissa.add(coordinates.getAbscissa()),
                this.getOrdinate().add(coordinates.getOrdinate()));
    }
}

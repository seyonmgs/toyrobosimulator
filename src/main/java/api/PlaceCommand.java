package api;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Optional;

/**
 * This represents the action that is to performed for a 'PLACE' command.
 *
 * @see Compass
 * @see Direction
 * @see ClCommand
 * @see Orientation
 */
public class PlaceCommand implements SimulatorCommand {

    /**
     * This carries out the action that is to be performed for a 'PLACE' command {@link ClCommand}.
     * If the provided orientation (via {@code arguments}) violates the constraints {@link SimulatorConstraints}, or
     * if the provided arguments are invalid co-ordinates and/or direction,
     * then the toyRobo will retain its current orientation (which may or may not be present).
     * @param toyRobo a ToyRobo {@link ToyRobo} object.
     * @param arguments an array of arguments provided with the command, if any.
     * @param constraints a SimulatorConstraints {@link SimulatorConstraints} object.
     * @param consoleWriter a ConsoleWriter {@link ConsoleWriter} to write responses to.
     */
    @Override
    public void action(ToyRobo toyRobo, String[] arguments, SimulatorConstraints constraints, ConsoleWriter consoleWriter) {
        Optional<Orientation> orientation = processAdditionalParams(arguments, constraints,consoleWriter);
        orientation.ifPresent(toyRobo::setOrientation);

    }

    /**
     * This carries out the validations of the arguments provided with a 'PLACE' command.
     * @param arguments an array of Strings holding the arguments provided.
     * @param constraints a SimulatorConstraints {@link SimulatorConstraints} object.
     * @param consoleWriter a ConsoleWriter {@link ConsoleWriter} to write responses to.
     * @return an Optional Orientation {@link Orientation} if the provided arguments are valid.
     */
    private Optional<Orientation> processAdditionalParams(
            String[] arguments, SimulatorConstraints constraints, ConsoleWriter consoleWriter) {

        String abscissa = arguments[0];
        String ordinate = arguments[1];

        if(!NumberUtils.isParsable(abscissa) | !NumberUtils.isParsable(ordinate)){
            consoleWriter.debug("Coordinate provided is not numeric.");
            return Optional.empty();
        }

        CartesianCoordinates coordinates = new CartesianCoordinates(abscissa, ordinate);

        if (!constraints.validateCoordinates(coordinates)) {
            consoleWriter.debug("Coordinate provided is not within constraints.");
            return Optional.empty();
        }

        Optional<Direction> givenDirection = Optional.ofNullable(EnumUtils.getEnum(Direction.class, arguments[2]));

        if (!givenDirection.isPresent()){
            consoleWriter.debug("Direction provided is invalid.");
            return Optional.empty();
        }

        consoleWriter.debug("Command accepted.");
        return givenDirection.map(direction -> new Orientation(coordinates, direction));

    }
}

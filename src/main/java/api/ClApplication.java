package api;

import java.util.Scanner;
/**
 * This is used to run the ToyRoboSimulator app from commandline.
 */
public class ClApplication {

    private static final Scanner scanner = new Scanner(System.in);
    private static final ClSimulator simulator = new ToyRoboClSimulator();

    public static void main(String[] args) {

        while (true){
            if(scanner.hasNext()){
                scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
                String commandLine = scanner.nextLine();
                simulator.simulate(commandLine);
            }
        }
    }


}

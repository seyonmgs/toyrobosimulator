package api;

import org.apache.commons.lang3.EnumUtils;

import java.util.EnumMap;
import java.util.Optional;

/**
 * This represents the ToyRobo CommandLine Simulator. This simulator is case-sensitive.
 */
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class ToyRoboClSimulator implements ClSimulator {

    /**
     * Delimiter to identify a Command in the string provided.
     */
    private final static String COMMAND_DELIMITER = " ";
    /**
     * Delimiter to identify a Command's arguments in the argument string provided.
     */
    private final static String ARGUMENT_DELIMITER = ",";

    /**
     * The minimum number of qualifiers (including argument string) which are to be provided
     * if the command expects arguments.
     */
    private final static int min_qualifiers = 1;

    /**
     * An EnumMap {@link EnumMap} that holds a map of valid Command Line commands that recognized
     * by this simulator and its corresponding command processor beans that carry out the action.
     */
    private final EnumMap<ClCommand, SimulatorCommand> processorMap;

    /**
     * The ToyRobo {@link ToyRobo} entity which is being simulated on this simulator.
     */
    private final ToyRobo toyRobo;

    /**LEFT
     * The constraints {@link SimulatorConstraints} of this simulator.
     */
    private final SimulatorConstraints simulatorConstraints;

    /**
     * The console writer {@link ConsoleWriter} used by this simulator to provide response/feedback to the user
     * interacting with this simulator via commandLine.
     */
    private final ConsoleWriter consoleWriter;

    /**
     * This constructs a simulator object.
     */
    public ToyRoboClSimulator() {
        this(configureCommandProcessorMap(), new ToyRobo(), new SimulatorConstraints(), new ConsoleWriter());
    }

    /**
     * This constructs a simulator object with the provided processorMap, toyRobo {@link ToyRobo},
     * constraints {@link SimulatorConstraints} and consoleWriter {@link ConsoleWriter}
     * Constraints and ConsoleWriter are configurable through system properties.
     */
    private ToyRoboClSimulator(EnumMap<ClCommand, SimulatorCommand> processorMap, ToyRobo toyRobo,
                               SimulatorConstraints simulatorConstraints, ConsoleWriter consoleWriter) {
        this.processorMap = processorMap;
        this.toyRobo = toyRobo;
        this.simulatorConstraints = simulatorConstraints;
        this.consoleWriter = consoleWriter;
    }

    /**
     * Constructs an map {@link EnumMap} of valid commands {@link ClCommand} and their corresponding
     * command processor beans.
     * @return an EnumMap.
     */
    private static EnumMap<ClCommand, SimulatorCommand> configureCommandProcessorMap() {
        EnumMap<ClCommand, SimulatorCommand> processorMap = new EnumMap<>(ClCommand.class);
        processorMap.put(ClCommand.PLACE, new PlaceCommand());
        processorMap.put(ClCommand.REPORT, new ReportCommand());
        processorMap.put(ClCommand.MOVE, new MoveCommand());
        processorMap.put(ClCommand.LEFT, new TurnLeftCommand());
        processorMap.put(ClCommand.RIGHT, new TurnRightCommand());
        return processorMap;
    }


    /**
     * Carries out the simulation action for this given string if it is a recognized command that meets all
     * its configured constraints.
     *
     * @param line a string representing the command line input provided by a user..
     */
    @Override
    public void simulate(String line) {

        Optional<ClCommand> optionalClCommand = this.retrieveCommand(line);

        //If not a recognized 'command', then ignore this input.
        if (!(optionalClCommand.isPresent())) return;

        ClCommand clCommand = optionalClCommand.get();
        Optional<String[]> arguments = retrieveArguments(clCommand,line);

        //If arguments are expected but none meeting the criteria are provided, then ignore this input.
        if (!arguments.isPresent()) return;

        Optional<SimulatorCommand> optionalSimulator = Optional.ofNullable(processorMap.get(clCommand));

        //If no bean configured for this command, then ignore the command.
        if (!(optionalSimulator.isPresent())) {
            consoleWriter.debug("Command processor not mapped.");
            return;
        }

        //carry out the simulation action.
        optionalSimulator.get().action(toyRobo, arguments.get(), simulatorConstraints, consoleWriter);
    }


    /**
     * Retrieve the arguments provided for along with this command. If none are expected for this command,
     * returns an empty array of strings.
     * If expected, the arguments are parsed and number of arguments is validated against the expected number.
     * If the numbers do not match, then the provided input is ignored and arguments are deemed as not provided.
     * @param line a string representing the command line input provided by a user..
     */
    private Optional<String[]> retrieveArguments(ClCommand clCommand, String line) {

        if (clCommand.getNoOfArguments() <= 0){
            return Optional.of(new String[0]);
        }

        String[] parsedStrings = line.trim().split(COMMAND_DELIMITER);

        if (parsedStrings.length == min_qualifiers) {
            consoleWriter.debug("Command Arguments expected but not provided.");
            return Optional.empty();
        }

        String[] arguments = parsedStrings[1].split(ARGUMENT_DELIMITER);

        if (arguments.length != clCommand.getNoOfArguments()) {
            consoleWriter.debug("Number of Arguments expected = " + clCommand.getNoOfArguments()
                    + " does not match provided number of arguments = " + arguments.length + ".");
            return Optional.empty();
        }

        return Optional.of(arguments);

    }


    /**
     * Retrieves the command identified by the provided string value.
     * @param line a String value.
     * @return an optional Command value.
     */
    private Optional<ClCommand> retrieveCommand(String line) {
        Optional<String> parsedStringValue = this.processLine(line);

        if (!(parsedStringValue.isPresent())) {
            consoleWriter.debug("Command not provided.");
            return Optional.empty();
        }


        Optional<ClCommand> optionalClCommand = this.getCommand(parsedStringValue.get());

        if (!(optionalClCommand.isPresent())) {
            consoleWriter.debug("Command undefined.");
            return Optional.empty();
        }

        String[] qualifiers = line.trim().split(COMMAND_DELIMITER);

        if (qualifiers.length > 2 || (qualifiers.length == 2
                &&  optionalClCommand.get().getNoOfArguments() == 0 )) {
            consoleWriter.debug("Command has unknown qualifiers.");
            return Optional.empty();
        }

        return optionalClCommand;
    }

    /**
     * A method that does a case sensitive matching of the provided string against the list
     * of Enum values {@link ClCommand}
     * @param value string value parsed from user input.
     * @return an optional ClCommand value.
     */
    private Optional<ClCommand> getCommand(String value) {
        return Optional.ofNullable(EnumUtils.getEnum(ClCommand.class, value));
    }

    /**
     * Parse and return command string.
     * @param line string value representing user input.
     * @return an optional String value.
     */
    private Optional<String> processLine(String line) {
        if (line == null || line.isEmpty()) return Optional.empty();
        return Optional.of(line.trim().split(COMMAND_DELIMITER)[0]);
    }


}

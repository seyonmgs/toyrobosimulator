package api;

import java.util.Optional;

/**
 * This represents the action that is to performed for a 'RIGHT' command.
 *
 * @see Compass
 * @see Direction
 * @see ClCommand
 * @see Orientation
 */
public class TurnRightCommand implements SimulatorCommand {
    /**
     * This method carries out the action that is to be performed for a 'RIGHT' command {@link ClCommand}.
     * If the toyRobo {@link ToyRobo} is not placed, no further action is carried out.
     * If the move violates the constraints {@link SimulatorConstraints}, then the toyRobo remains at its current
     * orientation.
     * @param toyRobo a ToyRobo {@link ToyRobo} object.
     * @param arguments an array of arguments provided with the command, if any.
     * @param constraints a SimulatorConstraints {@link SimulatorConstraints} object.
     * @param consoleWriter a ConsoleWriter {@link ConsoleWriter} to write responses to.
     */
    @Override
    public void action(ToyRobo toyRobo, String[] arguments, SimulatorConstraints constraints, ConsoleWriter consoleWriter) {
        Optional<Orientation> orientation = toyRobo.getOrientation();

        if (!(orientation.isPresent())) {
            consoleWriter.debug("Not placed. Cannot turn.");
            return;
        }

        CartesianCoordinates currentCoordinates = orientation.get().getCoordinates();
        Direction currentDirection = orientation.get().getDirection();

        Orientation newOrientation = new Orientation(currentCoordinates, Compass.turnRight(currentDirection));
        toyRobo.setOrientation(newOrientation);
        consoleWriter.debug("Command accepted.");
    }
}

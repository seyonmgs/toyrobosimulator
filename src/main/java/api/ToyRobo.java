package api;

import lombok.Getter;

import java.util.Optional;

/**
 * This class represents a ToyRobo. This is not serializable.
 *
 * @see  Orientation
 */
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
@Getter
public class ToyRobo {

    /**
     * Represents the current orientation of a ToyRobo. This is not empty only until its orientation is updated
     * This field is not serializable.
     * @see ClCommand
     */
    private Optional<Orientation> orientation;

    /**
     * Constructs an ToyRobo object with orientation empty.
     */
    public ToyRobo() {
        orientation = Optional.empty();
    }

    /**
     * Updates the orientation of this ToyRobo.
     */
    public void setOrientation(final Orientation newOrientation) {
        this.orientation = Optional.ofNullable(newOrientation);
    }

}

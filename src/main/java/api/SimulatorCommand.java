package api;

/**
 * <p>
 * An interface describing a simulator command.
 * </p>
 * This interface defines a generic protocol of a simulator command. It should be
 * sufficiently generic to be applied to multiple different use cases.
 * </p>
 *
 */
public interface SimulatorCommand {
    /**
     * Carries out the simulation action for this given command if it meets all the defined criteria and is within
     * its constraints.
     *
     * @param toyRobo a ToyRobo {@link ToyRobo} object.
     * @param arguments an array of arguments provided with the command, if any.
     * @param constraints a SimulatorConstraints {@link SimulatorConstraints} object.
     * @param consoleWriter a ConsoleWriter {@link ConsoleWriter} to write responses to.
     */
    void action(ToyRobo toyRobo, String[] arguments, SimulatorConstraints constraints, ConsoleWriter consoleWriter);
}

package api;

import lombok.Getter;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * This represents the physical constraints that define a Simulator's field of operation.
 *
 * @see CartesianCoordinates
 */
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
@Getter
public class SimulatorConstraints {

    /**
     * Property string that can be used to provide the max abscissa value.
     */
    private static final String CLTRS_MAX_ABSCISSA = "CLTRS_MAX_ABSCISSA";
    /**
     * Property string that can be used to provide the min abscissa value.
     */
    private static final String CLTRS_MIN_ABSCISSA = "CLTRS_MIN_ABSCISSA";
    /**
     * Property string that can be used to provide the min ordinate value.
     */
    private static final String CLTRS_MIN_ORDINATE = "CLTRS_MIN_ORDINATE";
    /**
     * Property string that can be used to provide the max ordinate value.
     */
    private static final String CLTRS_MAX_ORDINATE = "CLTRS_MAX_ORDINATE";
    /**
     * Default value that is set max abscissa and/or max ordinate when none is provided.
     */
    private static final String DEFAULT_MAX_VALUE = "5.0";
    /**
     * Default value that is set min abscissa and/or min ordinate when none is provided.
     */
    private static final String DEFAULT_MIN_VALUE = "0.0";
    /**
     * Represents the maximum abscissa value of a simulator's rectangular field of operation.
     */
    private final BigDecimal maxAbscissa;
    /**
     * Represents the maximum ordinate value of a simulator's rectangular field of operation.
     */
    private final BigDecimal maxOrdinate;
    /**
     * Represents the minimum abscissa value of a simulator's rectangular field of operation.
     */
    private final BigDecimal minAbscissa;
    /**
     * Represents the minimum ordinate value of a simulator's rectangular field of operation.
     */
    private final BigDecimal minOrdinate;

    /**
     * Constructs a SimulatorConstraints object.
     */
    public SimulatorConstraints() {
        this(deriveMaxAbscissa(), deriveMaxOrdinate(), deriveMinAbscissa(), deriveMinOrdinate());
    }

    /**
     * Constructs a SimulatorConstraints object using the parameters provided as BigDecimal values.
     * This creates a copy of the BigDecimal objects provided.
     *
     * @see BigDecimal
     */
    public SimulatorConstraints(BigDecimal maxAbscissa, BigDecimal maxOrdinate,
                                BigDecimal minAbscissa, BigDecimal minOrdinate) {
        this(maxAbscissa.toString(), maxOrdinate.toString(), minAbscissa.toString(), minOrdinate.toString());
    }


    /**
     * Constructs a SimulatorConstraints object using the parameters provided as String values.
     * This checks a constraint that respective min and max values are not the same. If yes, then it defaults the
     * min-max pair to their respective default values.
     *
     * @see BigDecimal
     */
    private SimulatorConstraints(String maxAbscissa, String maxOrdinate,
                                 String minAbscissa, String minOrdinate) {

        BigDecimal maxAbs = new BigDecimal(maxAbscissa, MathContext.UNLIMITED).setScale(2, RoundingMode.HALF_UP);
        BigDecimal maxOrd = new BigDecimal(maxOrdinate, MathContext.UNLIMITED).setScale(2, RoundingMode.HALF_UP);
        BigDecimal minAbs = new BigDecimal(minAbscissa, MathContext.UNLIMITED).setScale(2, RoundingMode.HALF_UP);
        BigDecimal minOrd = new BigDecimal(minOrdinate, MathContext.UNLIMITED).setScale(2, RoundingMode.HALF_UP);

        if (minAbs.compareTo(maxAbs) >= 0) {
            maxAbs = new BigDecimal(DEFAULT_MAX_VALUE, MathContext.UNLIMITED).setScale(2, RoundingMode.HALF_UP);
            minAbs = new BigDecimal(DEFAULT_MIN_VALUE, MathContext.UNLIMITED).setScale(2, RoundingMode.HALF_UP);
        }

        if (minOrd.compareTo(maxOrd) >= 0) {
            maxOrd = new BigDecimal(DEFAULT_MAX_VALUE, MathContext.UNLIMITED).setScale(2, RoundingMode.HALF_UP);
            minOrd = new BigDecimal(DEFAULT_MIN_VALUE, MathContext.UNLIMITED).setScale(2, RoundingMode.HALF_UP);
        }
        this.maxAbscissa = maxAbs;

        this.maxOrdinate = maxOrd;

        this.minAbscissa = minAbs;

        this.minOrdinate = minOrd;
    }

    /**
     * Derives the max abscissa value.
     */
    private static String deriveMaxAbscissa() {
        return deriveMaxValue(CLTRS_MAX_ABSCISSA);
    }

    /**
     * Derives the min abscissa value.
     */
    private static String deriveMinAbscissa() {
        return deriveMinValue(CLTRS_MIN_ABSCISSA);
    }

    /**
     * Derives the min ordinate value.
     */
    private static String deriveMinOrdinate() {
        return deriveMinValue(CLTRS_MIN_ORDINATE);
    }

    /**
     * Derives the min ordinate value.
     */
    private static String deriveMaxOrdinate() {
        return deriveMaxValue(CLTRS_MAX_ORDINATE);
    }

    /**
     * Derives the max value for the given property.
     * If the value set for the property is not numeric, then returns a default value.
     */
    private static String deriveMaxValue(String value) {
        String coordinate = System.getProperty(value);
        if (coordinate == null) return DEFAULT_MAX_VALUE;
        return NumberUtils.isParsable(coordinate) ? coordinate : DEFAULT_MAX_VALUE;
    }

    /**
     * Derives the min value for the given property.
     * If the value set for the property is not numeric, then returns a default value.
     */
    private static String deriveMinValue(String value) {
        String coordinate = System.getProperty(value);
        if (coordinate == null) return DEFAULT_MIN_VALUE;
        return NumberUtils.isParsable(coordinate) ? coordinate : DEFAULT_MIN_VALUE;
    }


    /**
     * Checks to see if the provided coordinates lie within or exactly on the perimeter of
     * the rectangular area of operation defined by the simulator constrains object.
     *
     * @return a boolean value false if it is outside the perimeter. Else, true.
     */
    public boolean validateCoordinates(CartesianCoordinates coordinates) {

        if (coordinates.getAbscissa().compareTo(maxAbscissa) > 0) {
            return false;
        }

        if (coordinates.getOrdinate().compareTo(maxOrdinate) > 0) {
            return false;
        }

        if (coordinates.getAbscissa().compareTo(minAbscissa) < 0) {
            return false;
        }

        return coordinates.getOrdinate().compareTo(minOrdinate) >= 0;
    }
}

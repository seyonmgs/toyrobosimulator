package api;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * This is an utility class to derive relative directions and
 * displacement measured in cartesian coordinates.
 *
 * This compass is oriented as shown below:
 *                     NORTH
 *                       |
 *               II      |      I
 *                       |
 *    WEST-------------------------------- EAST
 *                       |
 *               III     |      IV
 *                       |
 *                     SOUTH
 *
 *
 *
 * The four quadrants are distributed in an anti-clockwise pattern, with EAST at 0 degrees.
 *
 *
 * @see Direction
 * @see CartesianCoordinates
 *
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Compass {

    /**
     * This method computes the Direction to the left at a 90 degree
     * angle relative from a given direction.
     * @param currentDirection current Direction
     * @return Direction at 90 to currentDirection
     */
    public static Direction turnLeft(Direction currentDirection) {
        return doTurn(currentDirection, 90);
    }

    /**
     * This method computes the Direction to the right at a 90 degree
     * angle relative from a given direction.
     * @param currentDirection current Direction
     * @return Direction at 90 to currentDirection
     */
    public static Direction turnRight(Direction currentDirection) {
        return doTurn(currentDirection, -90);
    }


    /**
     * This method computes the Direction at the specified degree
     * relative from a given direction.
     *
     * If none found, sends back currentDirection.
     * @param currentDirection current Direction
     * @return Direction at specified degree from currentDirection
     */
    private static Direction doTurn(final Direction currentDirection, double degree) {
        double theta = currentDirection.getTheta() + degree;

        while (theta < 0) {
            theta = theta + 360 ;
        }

        while (theta >= 360) {
            theta = theta - 360;
        }


        Optional<Direction> newDirection = findDirectionFromTheta(theta);

        return newDirection.orElse(currentDirection);

    }


    /**
     * Finds the direction at the specified degree.
     * @param theta the specified degree/angle
     * @return the direction at the specified degree, if available.
     */
    private static Optional<Direction> findDirectionFromTheta(double theta) {
        return Arrays.stream(Direction.values()).filter(direction -> direction.getTheta() == theta).findAny();

    }


    /**
     * Finds the displacement, represented by a pair of cartesian coordinates,
     * required to move one unit forward in the specified direction.
     * @param direction current direction
     * @return a pair of cartesian coordinates.
     */
    public static CartesianCoordinates computeDisplacement(Direction direction){


        double theta = direction.getTheta();

        double thetaInRadians = Math.toRadians(theta);

        //assuming r is 1 unit, where x = r × cos(θ) and y = r × sin(θ)

        double x = Math.cos(thetaInRadians);
        double y = Math.sin(thetaInRadians);

        DecimalFormat formatter = new DecimalFormat("#.0#");

        return  new CartesianCoordinates(new BigDecimal(formatter.format(x)),new BigDecimal(formatter.format(y)));

    }

}

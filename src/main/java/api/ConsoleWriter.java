package api;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.EnumUtils;

import java.io.PrintStream;
import java.util.Optional;

/**
 * This class is used for providing response/feedback to a CommandLine user
 * interacting with a CommandLine Simulator.
 */

@SuppressWarnings("PMD.BeanMembersShouldSerialize")
@RequiredArgsConstructor
public class ConsoleWriter {
    /**
     * The property CLTRS_RESPONSE_LEVEL can be used to set the response level of the this
     * {@link ConsoleWriter}.
     */
    private static final String CLTRS_RESPONSE_LEVEL = "CLTRS_RESPONSE_LEVEL";
    /**
     * The response level of this {@link ConsoleWriter}.
     */
    private final Level level;

    /**
     * The output {@link PrintStream} this {@link ConsoleWriter} writes to.
     */
    private PrintStream outStream;

    /**
     * Constructs a new ConsoleWriter object. Defaults the output {@code outStream} to
     * System.out. Sets the response level to either the value specified by via the property
     * {@code CLTRS_RESPONSE_LEVEL} or INFO level if none is provided.
     */
    public ConsoleWriter() {
        this.level = deriveLevel();
        this.outStream = System.out;
    }

    /**
     * Constructs a new ConsoleWriter object. Defaults the output {@code outStream} to
     * System.out.
     */
    public ConsoleWriter(PrintStream outStream) {
        this();
        this.outStream = outStream;
    }

    /**
     * Constructs a new ConsoleWriter object. Sets the output {@code outStream} to the
     * user provided {@link PrintStream}.
     */
    public ConsoleWriter(Level level, PrintStream outStream) {
        this(level);
        this.outStream = outStream;
    }

    /**
     * Writes a 'INFO' level response message to this object's configured {@code outStream}.
     * user provided {@link PrintStream}.
     */
    public void info(String message) {
        this.doRespond(message, Level.INFO);
    }

    /**
     * Writes a 'DEBUG' level response message to this object's configured {@code outStream}.
     * user provided {@link PrintStream}.
     */
    public void debug(String message) {
        this.doRespond(message, Level.DEBUG);
    }

    /**
     * Writes a 'DEBUG' level response message to this object's configured {@code outStream}.
     * user provided {@link PrintStream}.
     */
    private void doRespond(String message, Level level) {
        if (level.value <= this.level.value) {
            this.outStream.println(message);
        }
    }

    /**
     * Derives the response Level.
     * Retrieves value for the property String CLTRS_RESPONSE_LEVEL and returns that as the response level,
     * if configured.
     * If the property is not set or if the value provided through the property is not a valid value, then
     * will return Level.INFO.
     *
     * @return Level
     */
    private Level deriveLevel() {

        String cltrs_response_level = System.getProperty(CLTRS_RESPONSE_LEVEL);

        if (cltrs_response_level == null) return Level.INFO;

        Optional<Level> optionalLevel = Optional.ofNullable(EnumUtils.getEnum(Level.class, cltrs_response_level));

        if (!(optionalLevel.isPresent())) return Level.INFO;

        if (optionalLevel.get().equals(Level.INFO)) return Level.INFO;
        return Level.DEBUG;

    }

    /**
     * This enum represents the valid response levels for this {@link ConsoleWriter}.
     */
    public enum Level {
        INFO(1),
        DEBUG(2);

        private final int value;

        Level(int value) {
            this.value = value;
        }

    }


}

package api;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Represents the Command Line commands that are recognized by a Command Line Simulator.
 */
@Getter
@RequiredArgsConstructor
public enum ClCommand {
    PLACE(3),
    MOVE(0),
    LEFT(0),
    RIGHT(0),
    REPORT(0);

    /**
     * Number of arguments that are expected to be provided for a given command.
     */
    private final int noOfArguments;
}

package api;

/**
 * <p>
 * An interface describing a simulator component.
 * </p>
 * This interface defines a generic protocol of a simulator. It should be
 * sufficiently generic to be applied to multiple different use cases.
 * </p>
 *
 */
public interface ClSimulator {
    /**
     * Processes the given input, identifies the command & arguments in it and carries out the simulation
     * mapped for the identified command.
     * @param line
     */
    void simulate(String line);
}

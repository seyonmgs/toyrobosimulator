package api;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class TurnRightCommandTest {

    private SimulatorConstraints constraints = new SimulatorConstraints(new BigDecimal(6), new BigDecimal(10),
            new BigDecimal(-1), new BigDecimal(-10));


    @Test
    void testTurnRightCommand() {
        ToyRobo toyRobo = new ToyRobo();

        TurnRightCommand turnCommand = new TurnRightCommand();

        assertThat("not placed", toyRobo.getOrientation().isPresent(), is(false));
        turnCommand.action(toyRobo, null, constraints, new ConsoleWriter());
        assertThat("not placed", toyRobo.getOrientation().isPresent(), is(false));

        Orientation orientation = new Orientation(new CartesianCoordinates("6", "3"), Direction.WEST);

        toyRobo.setOrientation(orientation);
        assertThat("placed", toyRobo.getOrientation().isPresent(), is(true));

        turnCommand.action(toyRobo, null, constraints, new ConsoleWriter());

        assertThat("direction has changed",
                toyRobo.getOrientation().get().getDirection(), is(Direction.NORTH));

        assertThat("ordinate remains same",
                toyRobo.getOrientation().get().getCoordinates().getOrdinate().toString(), is("3.00"));

        assertThat("abscissa remains same",
                toyRobo.getOrientation().get().getCoordinates().getAbscissa().toString(), is("6.00"));

        int i=0;
        while (i <9){
            //turning right 9 times starting from NORTH, must end up facing EAST
            turnCommand.action(toyRobo, null, constraints, new ConsoleWriter());
            i++;
        }

        assertThat("direction has changed after multiple moves",
                toyRobo.getOrientation().get().getDirection(), is(Direction.EAST));

        assertThat("ordinate remains same",
                toyRobo.getOrientation().get().getCoordinates().getOrdinate().toString(), is("3.00"));

        assertThat("abscissa remains same",
                toyRobo.getOrientation().get().getCoordinates().getAbscissa().toString(), is("6.00"));
    }
}

package api;

import org.apache.commons.lang3.EnumUtils;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ClCommandTest {

    @Test
    void checkPlace() {
        checkPresence("PLACE", 3);
    }

    @Test
    void checkMove() {
        checkPresence("MOVE", 0);
    }

    @Test
    void checkLeft() {
        checkPresence("LEFT", 0);
    }

    @Test
    void checkRight() {
        checkPresence("RIGHT", 0);
    }


    @Test
    void checkReport() {
        checkPresence("REPORT", 0);
    }

    @Test
    void onlyFiveCommandsAreAllowed() {
        assertThat(ClCommand.values().length, is(5));
    }


    private void checkPresence(String value, int noOfArguments) {
        Optional<ClCommand> optionalPlace = Optional.of(EnumUtils.getEnum(ClCommand.class, value));

        assertThat(optionalPlace.isPresent(), is(true));

        ClCommand place = optionalPlace.get();

        assertThat(place.toString(), is(value));
        assertThat(place.getNoOfArguments(), is(noOfArguments));
    }
}

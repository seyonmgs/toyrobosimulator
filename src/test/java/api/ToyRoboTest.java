package api;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ToyRoboTest {


    @Test
    void checkToyRobo_orientation() {
        ToyRobo toyRobo = new ToyRobo();

        assertThat("Orientation of new ToyRobo: ", toyRobo.getOrientation().isPresent(), is(false));

        CartesianCoordinates coordinates = new CartesianCoordinates(new BigDecimal(100), new BigDecimal(200));

        Orientation orientation = new Orientation(coordinates, Direction.SOUTH);
        toyRobo.setOrientation(orientation);

        assertThat("Orientation after first update: ", toyRobo.getOrientation().isPresent(), is(true));
        Orientation orientation1 = toyRobo.getOrientation().get();
        assertThat("X coordinate after first update: ",
                orientation1.getCoordinates().getAbscissa().intValue(), is(100));
        assertThat("Y coordinate after first update: ",
                orientation1.getCoordinates().getOrdinate().intValue(), is(200));
        assertThat("Direction after first update: ",
                orientation1.getDirection().toString(), is("SOUTH"));

        //is nullable again
        toyRobo.setOrientation(null);

        assertThat("Orientation after second update: ", toyRobo.getOrientation().isPresent(), is(false));


    }


}

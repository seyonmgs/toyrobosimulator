package api;

import org.junit.jupiter.api.Test;

import javax.print.DocFlavor;
import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SuppressWarnings({"PMD.AvoidDuplicateLiterals","PMD.BeanMembersShouldSerialize"})
class SimulatorConstraintsTest {

    private static final String CLTRS_MAX_ORDINATE = "CLTRS_MAX_ORDINATE";
    private static final String CLTRS_MIN_ORDINATE = "CLTRS_MIN_ORDINATE";
    private static final String CLTRS_MAX_ABSCISSA = "CLTRS_MAX_ABSCISSA";
    private static final String CLTRS_MIN_ABSCISSA = "CLTRS_MIN_ABSCISSA";

    @Test
    void testConstraints() {

        SimulatorConstraints simulatorConstraints = new SimulatorConstraints(new BigDecimal(5), new BigDecimal(6),
                new BigDecimal(0), new BigDecimal(-1));
        checkConstrains(simulatorConstraints, "5.00", "6.00", "-1.00", "0.00");

    }

    @Test
    void testCheckCoordinatesPositive() {

        SimulatorConstraints simulatorConstraints = new SimulatorConstraints(new BigDecimal(5), new BigDecimal(6),
                new BigDecimal(0), new BigDecimal(-2));

        CartesianCoordinates topRightCorner = new CartesianCoordinates(new BigDecimal(5), new BigDecimal(6));
        CartesianCoordinates lowerLeftCorner = new CartesianCoordinates(new BigDecimal(0), new BigDecimal(-2));
        CartesianCoordinates topLeftCorner = new CartesianCoordinates(new BigDecimal(0), new BigDecimal(6));
        CartesianCoordinates lowerRightCorner = new CartesianCoordinates(new BigDecimal(5), new BigDecimal(-2));
        CartesianCoordinates somewhereInside = new CartesianCoordinates(new BigDecimal(2), new BigDecimal(0));


        assertThat("topRightCorner",simulatorConstraints.validateCoordinates(topRightCorner), is(true));
        assertThat("lowerLeftCorner",simulatorConstraints.validateCoordinates(lowerLeftCorner), is(true));
        assertThat("topLeftCorner",simulatorConstraints.validateCoordinates(topLeftCorner), is(true));
        assertThat("lowerRightCorner",simulatorConstraints.validateCoordinates(lowerRightCorner), is(true));
        assertThat("somewhereInside",simulatorConstraints.validateCoordinates(somewhereInside), is(true));
    }

    @Test
    void testCheckCoordinatesNegative() {

        SimulatorConstraints simulatorConstraints = new SimulatorConstraints(new BigDecimal(5), new BigDecimal(6),
                new BigDecimal(0), new BigDecimal(-2));

        CartesianCoordinates outsideTopRightCorner = new CartesianCoordinates(new BigDecimal(6), new BigDecimal(6));
        CartesianCoordinates outsideLowerLeftCorner = new CartesianCoordinates(new BigDecimal(0), new BigDecimal(-3));
        CartesianCoordinates outsideTopLeftCorner = new CartesianCoordinates(new BigDecimal(-1), new BigDecimal(6));
        CartesianCoordinates outsideLowerRightCorner = new CartesianCoordinates(new BigDecimal(5), new BigDecimal(-5));
        CartesianCoordinates somewhereOutside = new CartesianCoordinates(new BigDecimal(-2), new BigDecimal(7));


        assertThat("outsideTopRightCorner", simulatorConstraints.validateCoordinates(outsideTopRightCorner), is(false));
        assertThat("outsideLowerLeftCorner", simulatorConstraints.validateCoordinates(outsideLowerLeftCorner), is(false));
        assertThat("outsideTopLeftCorner", simulatorConstraints.validateCoordinates(outsideTopLeftCorner), is(false));
        assertThat("outsideLowerRightCorner", simulatorConstraints.validateCoordinates(outsideLowerRightCorner), is(false));
        assertThat("somewhereOutside", simulatorConstraints.validateCoordinates(somewhereOutside), is(false));
    }


    @Test
    void testDefault_noProperties() {
        SimulatorConstraints simulatorConstraints = new SimulatorConstraints();
        checkConstrains(simulatorConstraints, "5.00", "5.00", "0.00", "0.00");
    }

    @Test
    void testDefault_onlyMaxOrdinatePropertySet() {
        System.setProperty(CLTRS_MAX_ORDINATE,"8.9");
        SimulatorConstraints simulatorConstraints = new SimulatorConstraints();
        checkConstrains(simulatorConstraints, "5.00", "8.90", "0.00", "0.00");
        System.clearProperty(CLTRS_MAX_ORDINATE);
    }

    @Test
    void testDefault_onlyMinOrdinatePropertySet() {
        System.setProperty(CLTRS_MIN_ORDINATE,"-1.9");
        SimulatorConstraints simulatorConstraints = new SimulatorConstraints();
        checkConstrains(simulatorConstraints, "5.00", "5.00", "-1.90", "0.00");
        System.clearProperty(CLTRS_MIN_ORDINATE);
    }

    @Test
    void testDefault_onlyMaxAbscissaPropertySet() {
        System.setProperty(CLTRS_MAX_ABSCISSA,"1.1");
        SimulatorConstraints simulatorConstraints = new SimulatorConstraints();
        checkConstrains(simulatorConstraints, "1.10", "5.00", "0.00", "0.00");
        System.clearProperty(CLTRS_MAX_ABSCISSA);
    }

    @Test
    void testDefault_onlyMinAbscissaPropertySet() {
        System.setProperty(CLTRS_MIN_ABSCISSA,"-2.9");
        SimulatorConstraints simulatorConstraints = new SimulatorConstraints();
        checkConstrains(simulatorConstraints, "5.00", "5.00", "0.00", "-2.90");
        System.clearProperty(CLTRS_MIN_ABSCISSA);
    }

    @Test
    void test_maxAbsNotGreaterThanMin_shouldFallBackToDefault() {
        System.setProperty(CLTRS_MIN_ABSCISSA,"-2.9");
        System.setProperty(CLTRS_MAX_ABSCISSA,"-3.0");
        System.setProperty(CLTRS_MAX_ORDINATE,"-3.0");
        System.setProperty(CLTRS_MIN_ORDINATE,"-5.0");
        SimulatorConstraints simulatorConstraints = new SimulatorConstraints();
        checkConstrains(simulatorConstraints, "5.00", "-3.00", "-5.00", "0.00");
        System.clearProperty(CLTRS_MIN_ABSCISSA);
        System.clearProperty(CLTRS_MAX_ABSCISSA);
        System.clearProperty(CLTRS_MAX_ORDINATE);
        System.clearProperty(CLTRS_MIN_ORDINATE);
    }

    @Test
    void test_maxOrdNotGreaterThanMin_shouldFallBackToDefault() {
        System.setProperty(CLTRS_MIN_ABSCISSA,"-2.9");
        System.setProperty(CLTRS_MAX_ABSCISSA,"7.0");
        System.setProperty(CLTRS_MAX_ORDINATE,"-3.0");
        System.setProperty(CLTRS_MIN_ORDINATE,"-3.0");
        SimulatorConstraints simulatorConstraints = new SimulatorConstraints();
        checkConstrains(simulatorConstraints, "7.00", "5.00", "0.00", "-2.90");
        System.clearProperty(CLTRS_MIN_ABSCISSA);
        System.clearProperty(CLTRS_MAX_ABSCISSA);
        System.clearProperty(CLTRS_MAX_ORDINATE);
        System.clearProperty(CLTRS_MIN_ORDINATE);
    }

    @Test
    void nonNumericValueProvided_shouldFallBackToDefault() {
        System.setProperty(CLTRS_MIN_ABSCISSA,"-2.9");
        System.setProperty(CLTRS_MAX_ABSCISSA,"AAB");
        System.setProperty(CLTRS_MAX_ORDINATE,"-3.0");
        System.setProperty(CLTRS_MIN_ORDINATE,"!@#");
        SimulatorConstraints simulatorConstraints = new SimulatorConstraints();
        checkConstrains(simulatorConstraints, "5.00", "5.00", "0.00", "-2.90");
        System.clearProperty(CLTRS_MIN_ABSCISSA);
        System.clearProperty(CLTRS_MAX_ABSCISSA);
        System.clearProperty(CLTRS_MAX_ORDINATE);
        System.clearProperty(CLTRS_MIN_ORDINATE);
    }

    void checkConstrains(SimulatorConstraints simulatorConstraints, String maxAbscissa, String maxOrdinate, String minOrdinate, String minAbscissa) {
        assertThat("max x axis value", simulatorConstraints.getMaxAbscissa().toString(), is(maxAbscissa));
        assertThat("max y axis value", simulatorConstraints.getMaxOrdinate().toString(), is(maxOrdinate));
        assertThat("min y axis value", simulatorConstraints.getMinOrdinate().toString(), is(minOrdinate));
        assertThat("min x axis value", simulatorConstraints.getMinAbscissa().toString(), is(minAbscissa));
    }
}
package api;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class TurnLeftCommandTest {

    private SimulatorConstraints constraints = new SimulatorConstraints(new BigDecimal(6), new BigDecimal(10),
            new BigDecimal(-1), new BigDecimal(-10));


    @Test
    void testTurnLeftCommand() {
        ToyRobo toyRobo = new ToyRobo();

        TurnLeftCommand turnCommand = new TurnLeftCommand();

        assertThat("not placed", toyRobo.getOrientation().isPresent(), is(false));
        turnCommand.action(toyRobo, null, constraints, new ConsoleWriter());
        assertThat("not placed", toyRobo.getOrientation().isPresent(), is(false));

        Orientation orientation = new Orientation(new CartesianCoordinates("1", "2"), Direction.NORTH);

        toyRobo.setOrientation(orientation);
        assertThat("placed", toyRobo.getOrientation().isPresent(), is(true));

        turnCommand.action(toyRobo, null, constraints, new ConsoleWriter());

        assertThat("direction has changed",
                toyRobo.getOrientation().get().getDirection(), is(Direction.WEST));

        assertThat("ordinate remains same",
                toyRobo.getOrientation().get().getCoordinates().getOrdinate().toString(), is("2.00"));

        assertThat("abscissa remains same",
                toyRobo.getOrientation().get().getCoordinates().getAbscissa().toString(), is("1.00"));

        int i=0;
        while (i <7){
            //turning left 7 times starting from WEST, must end up facing NORTH
            turnCommand.action(toyRobo, null, constraints, new ConsoleWriter());
            i++;
        }

        assertThat("direction has changed after multiple moves",
                toyRobo.getOrientation().get().getDirection(), is(Direction.NORTH));

        assertThat("ordinate remains same",
                toyRobo.getOrientation().get().getCoordinates().getOrdinate().toString(), is("2.00"));

        assertThat("abscissa remains same",
                toyRobo.getOrientation().get().getCoordinates().getAbscissa().toString(), is("1.00"));
    }
}

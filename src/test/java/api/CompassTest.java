package api;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class CompassTest {

    static final String X_COORDINATE = "x coordinate";
    static final String Y_COORDINATE = "y coordinate";
    @Test
    void facingEast_turnLeft(){
        Direction newDirection = Compass.turnLeft(Direction.EAST);
        assertThat(newDirection, is(Direction.NORTH));
    }

    @Test
    void facingEast_turnRight(){
        Direction newDirection = Compass.turnRight(Direction.EAST);
        assertThat(newDirection, is(Direction.SOUTH));
    }

    @Test
    void facingSouth_turnLeft(){
        Direction newDirection = Compass.turnLeft(Direction.SOUTH);
        assertThat(newDirection, is(Direction.EAST));
    }

    @Test
    void facingSouth_turnRight(){
        Direction newDirection = Compass.turnRight(Direction.SOUTH);
        assertThat(newDirection, is(Direction.WEST));
    }

    @Test
    void facingWest_turnLeft(){
        Direction newDirection = Compass.turnLeft(Direction.WEST);
        assertThat(newDirection, is(Direction.SOUTH));
    }

    @Test
    void facingWest_turnRight(){
        Direction newDirection = Compass.turnRight(Direction.WEST);
        assertThat(newDirection, is(Direction.NORTH));
    }

    @Test
    void facingNorth_turnLeft(){
        Direction newDirection = Compass.turnLeft(Direction.NORTH);
        assertThat(newDirection, is(Direction.WEST));
    }

    @Test
    void facingNorth_turnRight(){
        Direction newDirection = Compass.turnRight(Direction.NORTH);
        assertThat(newDirection, is(Direction.EAST));
    }

    @Test
    void facingEast_variationInCoordinates_whenMovingOneUnit(){
        CartesianCoordinates coordinates = Compass.computeDisplacement(Direction.EAST);

        assertThat(X_COORDINATE,coordinates.getAbscissa().intValue(), is(1));
        assertThat(Y_COORDINATE,coordinates.getOrdinate().intValue(), is(0));
    }

    @Test
    void facingWest_variationInCoordinates_whenMovingOneUnit(){
        CartesianCoordinates coordinates = Compass.computeDisplacement(Direction.WEST);
        assertThat(X_COORDINATE,coordinates.getAbscissa().intValue(), is(-1));
        assertThat(Y_COORDINATE,coordinates.getOrdinate().intValue(), is(0));
    }

    @Test
    void facingSouth_variationInCoordinates_whenMovingOneUnit(){
        CartesianCoordinates coordinates = Compass.computeDisplacement(Direction.SOUTH);
        assertThat(X_COORDINATE,coordinates.getAbscissa().intValue(), is(0));
        assertThat(Y_COORDINATE,coordinates.getOrdinate().intValue(), is(-1));
    }

    @Test
    void facingNorth_variationInCoordinates_whenMovingOneUnit(){
        CartesianCoordinates coordinates = Compass.computeDisplacement(Direction.NORTH);
        assertThat(X_COORDINATE,coordinates.getAbscissa().intValue(), is(0));
        assertThat(Y_COORDINATE,coordinates.getOrdinate().intValue(), is(1));
    }

}

package api;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SuppressWarnings("PMD.BeanMembersShouldSerialize")
class PlaceCommandTest {

    private SimulatorConstraints constraints = new SimulatorConstraints(new BigDecimal(5), new BigDecimal(5),
            new BigDecimal(-1), new BigDecimal(-1));


    @Test
    void testNegativeScenarios() {
        ToyRobo toyRobo = new ToyRobo();
        PlaceCommand command = new PlaceCommand();

        String[] invalidDirection = "-1,-1,NORTHE".split(",");
        command.action(toyRobo, invalidDirection, constraints, new ConsoleWriter());
        assertThat("invalid direction", toyRobo.getOrientation().isPresent(), is(false));

        String[] invalidCoordinates_notNumeric = "A,B,EAST".split(",");
        command.action(toyRobo, invalidCoordinates_notNumeric, constraints, new ConsoleWriter());
        assertThat("coordinates not numeric", toyRobo.getOrientation().isPresent(), is(false));

        String[] invalidCoordinates_outsideConstraints = "6,0,EAST".split(",");
        command.action(toyRobo, invalidCoordinates_outsideConstraints, constraints, new ConsoleWriter());
        assertThat("coordinates outside constraints", toyRobo.getOrientation().isPresent(), is(false));
    }

    @Test
    void testPositiveScenarios() {
        ToyRobo toyRobo = new ToyRobo();
        PlaceCommand command = new PlaceCommand();

        String[] validCommand_facingEast = "5,0,EAST".split(",");
        command.action(toyRobo, validCommand_facingEast, constraints,new ConsoleWriter());

        assertThat("facing east", toyRobo.getOrientation().isPresent(), is(true));
        assertThat("facing east x axis",
                toyRobo.getOrientation().get().getCoordinates().getAbscissa().toString(), is("5.00"));
        assertThat("facing east y axis",
                toyRobo.getOrientation().get().getCoordinates().getOrdinate().toString(), is("0.00"));
        assertThat("facing east direction",
                toyRobo.getOrientation().get().getDirection().toString(), is("EAST"));


        String[] validCommand_facingNorth = "-1,-1,NORTH".split(",");
        command.action(toyRobo, validCommand_facingNorth, constraints,new ConsoleWriter());

        assertThat("facing north", toyRobo.getOrientation().isPresent(), is(true));
        assertThat("facing north x axis",
                toyRobo.getOrientation().get().getCoordinates().getAbscissa().toString(), is("-1.00"));
        assertThat("facing north y axis",
                toyRobo.getOrientation().get().getCoordinates().getOrdinate().toString(), is("-1.00"));
        assertThat("facing north direction",
                toyRobo.getOrientation().get().getDirection().toString(), is("NORTH"));


        String[] validCommand_facingSouth = "-1,5,SOUTH".split(",");
        command.action(toyRobo, validCommand_facingSouth, constraints,new ConsoleWriter());

        assertThat("facing south", toyRobo.getOrientation().isPresent(), is(true));
        assertThat("facing south x axis",
                toyRobo.getOrientation().get().getCoordinates().getAbscissa().toString(), is("-1.00"));
        assertThat("facing south y axis",
                toyRobo.getOrientation().get().getCoordinates().getOrdinate().toString(), is("5.00"));
        assertThat("facing south direction",
                toyRobo.getOrientation().get().getDirection().toString(), is("SOUTH"));

        String[] validCommand_facingWest = "5,0,WEST".split(",");
        command.action(toyRobo, validCommand_facingWest, constraints,new ConsoleWriter());

        assertThat("facing west", toyRobo.getOrientation().isPresent(), is(true));
        assertThat("facing west x axis",
                toyRobo.getOrientation().get().getCoordinates().getAbscissa().toString(), is("5.00"));
        assertThat("facing west y axis",
                toyRobo.getOrientation().get().getCoordinates().getOrdinate().toString(), is("0.00"));
        assertThat("facing west direction",
                toyRobo.getOrientation().get().getDirection().toString(), is("WEST"));
    }

}
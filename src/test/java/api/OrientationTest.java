package api;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
public class OrientationTest {

    @Test
    void checkPosition(){

        CartesianCoordinates coordinates = new CartesianCoordinates(new BigDecimal(1), new BigDecimal(100));

        Orientation orientation = new Orientation(coordinates, Direction.EAST);

        assertThat("X Coordinate", orientation.getCoordinates().getAbscissa().intValue(), is(1));
        assertThat("Y Coordinate", orientation.getCoordinates().getOrdinate().intValue(), is(100));
        assertThat("Facing direction", orientation.getDirection().toString(), is("EAST"));
    }

    @Test
    void checkNPE(){

        CartesianCoordinates coordinates = new CartesianCoordinates(new BigDecimal(1), new BigDecimal(100));
        Assertions.assertThrows(NullPointerException.class,() ->new Orientation(coordinates, null));
        Assertions.assertThrows(NullPointerException.class,() ->new Orientation(null, Direction.EAST));

    }
}

package api;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;

import static api.ConsoleWriterTest.configureClResponse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.startsWith;

@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class ReportCommandTest {


    final String CLTRS_RESPONSE_LEVEL = "CLTRS_RESPONSE_LEVEL";
    final String NOT_PLACED_NOTHING_TO_REPORT = "Not placed. Nothing to report.";

    @Test
    void toyRobo_notPlacedYet_nothingToReport() {

        ReportCommand reportCommand = new ReportCommand();
        System.setProperty(CLTRS_RESPONSE_LEVEL, "DEBUG");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ConsoleWriter consoleWriter = configureClResponse(baos);

        reportCommand.action(new ToyRobo(), null, null, consoleWriter);

        assertThat(new String(baos.toByteArray()), startsWith(NOT_PLACED_NOTHING_TO_REPORT));
        System.clearProperty(CLTRS_RESPONSE_LEVEL);
    }

    @Test
    void toyRobo_placed_henceReports() {

        ReportCommand reportCommand = new ReportCommand();
        System.setProperty(CLTRS_RESPONSE_LEVEL, "DEBUG");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ConsoleWriter consoleWriter = configureClResponse(baos);

        ToyRobo toyRobo = new ToyRobo();
        CartesianCoordinates coordinates = new CartesianCoordinates("1", "3");
        toyRobo.setOrientation(new Orientation(coordinates,Direction.NORTH));
        reportCommand.action(toyRobo, null, null, consoleWriter);

        assertThat(new String(baos.toByteArray()), startsWith("1,3,NORTH"));
        System.clearProperty(CLTRS_RESPONSE_LEVEL);
    }
}

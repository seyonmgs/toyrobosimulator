package api;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class MoveCommandTest {

    private SimulatorConstraints constraints = new SimulatorConstraints(new BigDecimal(5), new BigDecimal(5),
            new BigDecimal(-1), new BigDecimal(-1));


    @Test
    void testMoveCommand() {
        ToyRobo toyRobo = new ToyRobo();
        MoveCommand moveCommand = new MoveCommand();

        assertThat("not placed", toyRobo.getOrientation().isPresent(), is(false));
        moveCommand.action(toyRobo, null, constraints, new ConsoleWriter());
        assertThat("not placed", toyRobo.getOrientation().isPresent(), is(false));

        Orientation orientation = new Orientation(new CartesianCoordinates("1", "2"), Direction.NORTH);

        toyRobo.setOrientation(orientation);
        assertThat("placed", toyRobo.getOrientation().isPresent(), is(true));

        moveCommand.action(toyRobo, null, constraints, new ConsoleWriter());

        assertThat("direction is same as before",
                toyRobo.getOrientation().get().getDirection(), is(Direction.NORTH));

        assertThat("ordinate increased by 1",
                toyRobo.getOrientation().get().getCoordinates().getOrdinate().toString(), is("3.00"));

        assertThat("abscissa remains same",
                toyRobo.getOrientation().get().getCoordinates().getAbscissa().toString(), is("1.00"));

        int i=0;
        while (i <7){
            //multiple moves that if allowed would exceed constraint limit of 5
            moveCommand.action(toyRobo, null, constraints, new ConsoleWriter());
            i++;
        }

        assertThat("direction is same as before after multiple moves",
                toyRobo.getOrientation().get().getDirection(), is(Direction.NORTH));

        assertThat("ordinate increased, but does not exceed max ordinate",
                toyRobo.getOrientation().get().getCoordinates().getOrdinate().toString(), is("5.00"));

        assertThat("abscissa remains same",
                toyRobo.getOrientation().get().getCoordinates().getAbscissa().toString(), is("1.00"));
    }
}

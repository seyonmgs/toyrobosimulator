package api;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.startsWith;

@SuppressWarnings({"PMD.BeanMembersShouldSerialize", "PMD.AvoidDuplicateLiterals",
        "PMD.DataflowAnomalyAnalysis"})
public class ToyRoboClSimulatorTest {

    private PrintStream originalStream;
    private ByteArrayOutputStream outputStream;
    private static final String REPORT = "REPORT";
    private static final String MOVE = "MOVE";
    private static final String LEFT = "LEFT";
    private static final String RIGHT = "RIGHT";
    private static final String NOT_PLACED_NOTHING_TO_REPORT = "Not placed. Nothing to report.";
    private static final String NOT_PLACED_CANNOT_MOVE = "Not placed. Cannot move.";
    private static final String NOT_PLACED_CANNOT_TURN = "Not placed. Cannot turn.";
    private static final String COMMAND_ACCEPTED = "Command accepted.";
    private static final String CONSTRAINT_VIOLATION_CANNOT_MOVE = "Constraint violation. Cannot move.";

    @BeforeEach
    void redirectSystemOutToTestOutput() {
        outputStream = new ByteArrayOutputStream();
        originalStream = System.out;
        System.setOut(new PrintStream(outputStream));
    }

    @AfterEach
    void resetSystemOutStream() {
        System.setOut(originalStream);
    }

    @Test
    void validPlaceCommand_toyRoboMustAcceptAndBePlaced() {
        ToyRoboClSimulator defaultSimulator = new ToyRoboClSimulator();
        String line1 = "PLACE 1,5,EAST";
        defaultSimulator.simulate(line1);
        defaultSimulator.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("1,5,EAST"));
    }

    @Test
    void unknownCommand_toyRoboMustNotBePlaced() {
        ToyRoboClSimulator simulatorWithDebug = getSimulatorWithDebug();
        String line1 = "XYZ 1,5,EAST";

        simulatorWithDebug.simulate(line1);
        assertThat(new String(outputStream.toByteArray()), startsWith("Command undefined."));

        outputStream.reset();

        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith(NOT_PLACED_NOTHING_TO_REPORT));
    }

    @Test
    void noCommandProvided() {
        ToyRoboClSimulator simulatorWithDebug = getSimulatorWithDebug();

        simulatorWithDebug.simulate(null);
        assertThat(new String(outputStream.toByteArray()), startsWith("Command not provided."));

        outputStream.reset();

        simulatorWithDebug.simulate("");
        assertThat(new String(outputStream.toByteArray()), startsWith("Command not provided."));
    }

    @Test
    void commandExpectsArguments_butNoneProvided(){
        ToyRoboClSimulator simulatorWithDebug = getSimulatorWithDebug();
        String line1 = "PLACE";

        simulatorWithDebug.simulate(line1);
        assertThat(new String(outputStream.toByteArray()), startsWith("Command Arguments expected but not provided."));

        outputStream.reset();

        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith(NOT_PLACED_NOTHING_TO_REPORT));
    }

    @Test
    void commandExpectsArguments_moreThanExpectedArgumentsProvided(){
        ToyRoboClSimulator simulatorWithDebug = getSimulatorWithDebug();
        String line1 = "PLACE 1,2,EAST,1";

        simulatorWithDebug.simulate(line1);
        assertThat(new String(outputStream.toByteArray()),
                startsWith("Number of Arguments expected = 3 does not match provided number of arguments = 4."));

        outputStream.reset();

        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith(NOT_PLACED_NOTHING_TO_REPORT));
    }

    @Test
    void knownCommandsWithUnknownQualifiers_mustBeIgnored(){
        ToyRoboClSimulator simulatorWithDebug = getSimulatorWithDebug();
        String line1 = "PLACE 1,2,EAST BLA BLA";
        String REPORT_INVALID = "REPORT BLA";

        simulatorWithDebug.simulate(line1);
        assertThat(new String(outputStream.toByteArray()),
                startsWith("Command has unknown qualifiers."));

        outputStream.reset();

        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith(NOT_PLACED_NOTHING_TO_REPORT));

        outputStream.reset();

        simulatorWithDebug.simulate(REPORT_INVALID);
        assertThat(new String(outputStream.toByteArray()),
                startsWith("Command has unknown qualifiers."));

    }

    @Test
    void mustAcceptValidPlaceCommands_IgnoringInvalidPlaceCommand(){
        ToyRoboClSimulator simulatorWithDebug = getSimulatorWithDebug();
        String valid1 = "PLACE 1,2,EAST";
        String valid2 = "PLACE 2,4,WEST";
        String valid3 = "PLACE 5,0,SOUTH";
        String invalid = "PLACE 6,0,NORTH";

        simulatorWithDebug.simulate(valid1);
        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("1,2,EAST"));

        simulatorWithDebug.simulate(valid2);
        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);

        assertThat(new String(outputStream.toByteArray()), startsWith("2,4,WEST"));

        simulatorWithDebug.simulate(valid3);
        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);

        assertThat(new String(outputStream.toByteArray()), startsWith("5,0,SOUTH"));

        simulatorWithDebug.simulate(invalid);
        outputStream.reset();

        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("5,0,SOUTH"));

    }

    @Test
    void mustIgnoreAllCommandsExceptReportUntilPlaced() {

        ToyRoboClSimulator simulatorWithDebug = getSimulatorWithDebug();

        int i = 0;
        while (i < 9) {
            outputStream.reset();
            simulatorWithDebug.simulate(REPORT);
            assertThat("i is " + i, new String(outputStream.toByteArray()), startsWith(NOT_PLACED_NOTHING_TO_REPORT));

            outputStream.reset();
            simulatorWithDebug.simulate(MOVE);
            assertThat("i is " + i, new String(outputStream.toByteArray()), startsWith(NOT_PLACED_CANNOT_MOVE));

            outputStream.reset();
            simulatorWithDebug.simulate(REPORT);
            assertThat("i is " + i, new String(outputStream.toByteArray()), startsWith(NOT_PLACED_NOTHING_TO_REPORT));

            outputStream.reset();
            simulatorWithDebug.simulate(LEFT);
            assertThat("i is " + i, new String(outputStream.toByteArray()), startsWith(NOT_PLACED_CANNOT_TURN));

            outputStream.reset();
            simulatorWithDebug.simulate(REPORT);
            assertThat("i is " + i, new String(outputStream.toByteArray()), startsWith(NOT_PLACED_NOTHING_TO_REPORT));

            outputStream.reset();
            simulatorWithDebug.simulate(RIGHT);
            assertThat("i is " + i, new String(outputStream.toByteArray()), startsWith(NOT_PLACED_CANNOT_TURN));

            outputStream.reset();
            simulatorWithDebug.simulate(REPORT);
            assertThat("i is " + i, new String(outputStream.toByteArray()), startsWith(NOT_PLACED_NOTHING_TO_REPORT));

            i++;

        }
    }


    @Test
    void mustAcceptAllCommandsBeingPlaced() {

        ToyRoboClSimulator simulatorWithDebug = getSimulatorWithDebug();

        int i = 0;
        while (i < 4) {

            String valid1 = "PLACE 1,2,EAST";
            simulatorWithDebug.simulate(valid1);

            outputStream.reset();
            simulatorWithDebug.simulate(REPORT);
            assertThat("i is " + i, new String(outputStream.toByteArray()), startsWith("1,2,EAST"));

            outputStream.reset();
            simulatorWithDebug.simulate(MOVE);
            assertThat("i is " + i, new String(outputStream.toByteArray()), startsWith(COMMAND_ACCEPTED));

            outputStream.reset();
            simulatorWithDebug.simulate(REPORT);
            assertThat("i is " + i, new String(outputStream.toByteArray()), startsWith("2,2,EAST"));

            outputStream.reset();
            simulatorWithDebug.simulate(LEFT);
            assertThat("i is " + i, new String(outputStream.toByteArray()), startsWith(COMMAND_ACCEPTED));

            outputStream.reset();
            simulatorWithDebug.simulate(REPORT);
            assertThat("i is " + i, new String(outputStream.toByteArray()), startsWith("2,2,NORTH"));

            outputStream.reset();
            simulatorWithDebug.simulate(RIGHT);
            assertThat("i is " + i, new String(outputStream.toByteArray()), startsWith(COMMAND_ACCEPTED));

            outputStream.reset();
            simulatorWithDebug.simulate(REPORT);
            assertThat("i is " + i, new String(outputStream.toByteArray()), startsWith("2,2,EAST"));

            i++;

        }
    }


    @Test
    void movingAlongPerimeter_antiClockWise() {

        ToyRoboClSimulator simulatorWithDebug = getSimulatorWithDebug();

        String valid1 = "PLACE 0,0,EAST";
        simulatorWithDebug.simulate(valid1);

        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("0,0,EAST"));


        move5Times(simulatorWithDebug,"EAST");

        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("5,0,EAST"));


        cannotMove5Times(simulatorWithDebug, "EAST");

        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("5,0,EAST"));


        simulatorWithDebug.simulate(LEFT);

        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("5,0,NORTH"));


        move5Times(simulatorWithDebug,"NORTH");

        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("5,5,NORTH"));

        cannotMove5Times(simulatorWithDebug, "NORTH");

        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("5,5,NORTH"));

        simulatorWithDebug.simulate(LEFT);

        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("5,5,WEST"));

        move5Times(simulatorWithDebug,"WEST");

        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("0,5,WEST"));

        cannotMove5Times(simulatorWithDebug, "WEST");

        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("0,5,WEST"));


        simulatorWithDebug.simulate(LEFT);

        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("0,5,SOUTH"));


        move5Times(simulatorWithDebug,"SOUTH");

        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("0,0,SOUTH"));

        cannotMove5Times(simulatorWithDebug, "SOUTH");

        outputStream.reset();
        simulatorWithDebug.simulate(REPORT);
        assertThat(new String(outputStream.toByteArray()), startsWith("0,0,SOUTH"));

    }

    private void cannotMove5Times(ToyRoboClSimulator simulatorWithDebug, String direction ) {
        int i=0;
        while (i < 5) {


            outputStream.reset();
            simulatorWithDebug.simulate(MOVE);
            assertThat("Cannot move further "+ direction + " " + i, new String(outputStream.toByteArray()),
                    startsWith(CONSTRAINT_VIOLATION_CANNOT_MOVE));

            i++;
        }
    }

    private void move5Times(ToyRoboClSimulator simulatorWithDebug, String direction) {
        int i = 0;
        while (i < 5) {


            outputStream.reset();
            simulatorWithDebug.simulate(MOVE);
            assertThat("Moving " +direction + " " + i, new String(outputStream.toByteArray()),
                    startsWith(COMMAND_ACCEPTED));

            i++;
        }
    }


    private ToyRoboClSimulator getSimulatorWithDebug() {
        System.setProperty("CLTRS_RESPONSE_LEVEL", "DEBUG");
        ToyRoboClSimulator simulator = new ToyRoboClSimulator();
        System.clearProperty("CLTRS_RESPONSE_LEVEL");
        return simulator;
    }

}

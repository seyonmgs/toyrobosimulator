package api;


import org.apache.commons.lang3.EnumUtils;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class DirectionsTest {


    @Test
    void checkNorth() {
        testPresence("NORTH", 90);
    }

    @Test
    void checkSouth() {
        testPresence("SOUTH", 270);
    }

    @Test
    void checkEast() {
        testPresence("EAST", 0);
    }

    @Test
    void checkWest() {
        testPresence("WEST", 180);
    }

    @Test
    void onlyFourDirectionsAreAllowed() {
        assertThat(Direction.values().length, is(4));
    }

    private void testPresence(String value, double degree) {
        Optional<Direction> optionalDirection = Optional.of(EnumUtils.getEnum(Direction.class, value));
        assertThat("Enum present", optionalDirection.isPresent(), is(true));


        assertThat("Theta/degree for this direction", optionalDirection.get().getTheta(), is(degree));
    }

}

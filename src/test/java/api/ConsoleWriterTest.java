package api;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static api.ConsoleWriter.Level;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;

@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class ConsoleWriterTest {

    static final String CLTRS_RESPONSE_LEVEL = "CLTRS_RESPONSE_LEVEL";

    @Test
    void testLogLevelThruProperty_paramProvidedAsInfo() {
        System.setProperty(CLTRS_RESPONSE_LEVEL, "INFO");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ConsoleWriter consoleWriter = configureClResponse(baos);

        mustWriteInfoLevelMessage(baos, consoleWriter);
        mustNotWriteDebug();
        System.clearProperty(CLTRS_RESPONSE_LEVEL);

    }

    private void mustNotWriteDebug() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ConsoleWriter consoleWriter = configureClResponse(baos);
        mustNotWriteDebugLevelMessage(baos, consoleWriter);
    }

    @Test
    void testLogLevelThruProperty_paramProvidedAsDebug() {
        System.setProperty(CLTRS_RESPONSE_LEVEL, "DEBUG");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ConsoleWriter consoleWriter = configureClResponse(baos);
        mustWriteDebugLevelMessage(baos, consoleWriter);
        System.clearProperty(CLTRS_RESPONSE_LEVEL);
    }

    @Test
    void testLogLevelThruProperty_paramProvidedIsUnknown() {
        System.setProperty(CLTRS_RESPONSE_LEVEL, "UNKNOWN");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ConsoleWriter consoleWriter = configureClResponse(baos);
        mustWriteInfoLevelMessage(baos, consoleWriter);
        mustNotWriteDebug();
        System.clearProperty(CLTRS_RESPONSE_LEVEL);
    }


    @Test
    void testLogLevel_propertyNotSet_mustBeInfoLevel() {
        System.clearProperty(CLTRS_RESPONSE_LEVEL);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ConsoleWriter consoleWriter = configureClResponse(baos);
        mustWriteInfoLevelMessage(baos, consoleWriter);
        mustNotWriteDebug();
    }


    public static ConsoleWriter configureClResponse(ByteArrayOutputStream baos) {
        PrintStream outputPrintStream = new PrintStream(baos);
        return new ConsoleWriter(outputPrintStream);
    }

    @Test
    void testInfoLevel_mustWriteInfoLevelMessage() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream outputPrintStream = new PrintStream(baos);
        ConsoleWriter consoleWriter = new ConsoleWriter(Level.INFO, outputPrintStream);

        mustWriteInfoLevelMessage(baos, consoleWriter);

    }

    @Test
    void testInfoLevel_mustNotWriteDebugLevelMessage() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream outputPrintStream = new PrintStream(baos);
        ConsoleWriter consoleWriter = new ConsoleWriter(Level.INFO, outputPrintStream);

        mustNotWriteDebugLevelMessage(baos, consoleWriter);

    }

    private void mustNotWriteDebugLevelMessage(ByteArrayOutputStream baos, ConsoleWriter consoleWriter) {
        String message = "test 1 test 2";
        consoleWriter.debug(message);

        assertThat(baos.size(), is(0));
    }

    @Test
    void testDebugLevel_mustWriteInfoLevelMessage() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream outputPrintStream = new PrintStream(baos);
        ConsoleWriter consoleWriter = new ConsoleWriter(Level.DEBUG, outputPrintStream);

        mustWriteInfoLevelMessage(baos, consoleWriter);

    }

    private void mustWriteInfoLevelMessage(ByteArrayOutputStream baos, ConsoleWriter consoleWriter) {
        String message = "test 1 test 2";
        consoleWriter.info(message);

        assertThat(new String(baos.toByteArray()), startsWith(message));
    }

    @Test
    void testDebugLevel_mustWriteDebugLevelMessage() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream outputPrintStream = new PrintStream(baos);
        ConsoleWriter consoleWriter = new ConsoleWriter(Level.DEBUG, outputPrintStream);

        mustWriteDebugLevelMessage(baos, consoleWriter);

    }

    private void mustWriteDebugLevelMessage(ByteArrayOutputStream baos, ConsoleWriter consoleWriter) {
        String message = "test 1 test 2";
        consoleWriter.debug(message);

        assertThat(new String(baos.toByteArray()), startsWith(message));
    }
}

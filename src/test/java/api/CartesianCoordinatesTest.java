package api;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class CartesianCoordinatesTest {

    @Test
    void checkPositiveValues(){
        BigDecimal xCoordinate = new BigDecimal("10.935");
        BigDecimal yCoordinate = new BigDecimal("11");
        CartesianCoordinates cartesianCoordinates = new CartesianCoordinates(xCoordinate, yCoordinate);

        assertThat(cartesianCoordinates.getAbscissa().toString(), is("10.94"));
        assertThat(cartesianCoordinates.getOrdinate().toString(), is("11.00"));
    }
}
